import React, { useState, useEffect } from "react";
import { ethers } from "ethers";
import "./App.css";
import Lock from "./artifacts/contracts/Lock.sol/Lock.json";

// mettre l'addresse de hardhat dans lock addresse
const LockAdress = "";

function App() {
  const [lock, setLockValue] = useState();

  //function update
  async function requestAccount() {
    await window.ethereum.request({ method: "eth_requestAccounts" });
  }

  async function fetchGreeting() {
    if (typeof window.ethereum !== "undefined") {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const contract = new ethers.Contract(LockAdress, Lock.abi, provider);
      try {
        const data = await contract.withdraw();
        setLockValue(data);
        console.log(data);
      } catch (error) {
        console.log(error);
      }
    }
  }
  useEffect(() => {
    fetchGreeting();
  });
  return (
    <div className="App">
      <header className="App-header">
        <p>{lock}</p>
      </header>
    </div>
  );
}

export default App;
